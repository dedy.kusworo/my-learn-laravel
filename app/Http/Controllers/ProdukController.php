<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = DB::table('tbproduk')->get();//(tanpa model)
        //return $users;
        //return view('data.dataindex',['users'=>$users]);
        return view('data.dataproduct',['users'=>$users]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('data.nambahproduct');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $crud=new Produk;
        $crud->namaproduk=$request->produk;
        $crud->kodeproduk=$request->kodeproduk;
        $crud->harga=$request->harga;
        $crud->satuan=$request->satuan;
        $crud->save();
        return redirect('/produk');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $users = DB::table('tbproduk')->where('id',$id)->first();//(tanpa model)
        //return compact('users');
        return view('data.detailproduk',compact('users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users = DB::table('tbproduk')->where('id',$id)->first();//(tanpa model)
        //return compact('users');
        return view('data.ngubahproduk',compact('users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Produk $produk)
    {
        //
        DB::table('tbproduk')->where('id',$request->id)->update([
            'namaproduk'=>$request->produk,
            'kodeproduk'=>$request->kodeproduk,
            'satuan'=>$request->satuan,
            'harga'=>$request->harga
        ]);
       return redirect('/produk');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('tbproduk')->where('id',$id)->delete();
        return redirect('/produk');
    }
}
