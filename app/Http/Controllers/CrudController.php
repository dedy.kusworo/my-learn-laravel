<?php

namespace App\Http\Controllers;

use App\Models\Crud;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class CrudController extends Controller
{
    public function index(){
        $users = DB::table('laptop')->get();//(tanpa model)
        //return $users;
        //return view('data.dataindex',['users'=>$users]);
        return view('data.data',['users'=>$users]);
    }
    public function create(){
        // view('data.datacreate');
        return view('data.nambah');
    }
    public function store(Request $request){
        $crud=new Crud;
        $crud->name=$request->name;
        $crud->suplayer=$request->suplayer;
        $crud->pemesan=$request->pemesan;
        $crud->save();
        return redirect('/show');
    }
    public function edit($id){
        $users = DB::table('laptop')->where('id',$id)->first();//(tanpa model)
        //return view('data.dataedit',compact('users'));
        return view('data.ngubah',compact('users'));
        //return view('data.dataindex',['users'=>$users]);
        //return view('data.dataedit',compact($crud));
    }
    public function update(Request $request){
        DB::table('laptop')->where('id',$request->id)->update([
            'name'=>$request->name,
            'pemesan'=>$request->pemesan,
            'suplayer'=>$request->suplayer
        ]);
       return redirect('/show');
       // return $request->all();
    }
    public function destroy($id){
        DB::table('laptop')->where('id',$id)->delete();
        return redirect('/show');
    }

}
