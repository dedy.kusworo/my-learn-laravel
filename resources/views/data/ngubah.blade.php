@extends('dashboard')
@section('head','Tambah Data')
@section('konten')
<!--div class="p-6 sm:px-20 bg-white border-b border-gray-200">
    <div>
        <x-jet-application-logo class="block h-12 w-auto" />
    </div>

    <div class="mt-8 text-2xl">
        Welcome to your Jetstream application! hello
    </div>

    <div class="mt-6 text-gray-500">
        Laravel Jetstream provides a beautiful, robust starting point for your next Laravel application. Laravel is designed
        to help you build your application using a development environment that is simple, powerful, and enjoyable. We believe
        you should love expressing your creativity through programming, so we have spent time carefully crafting the Laravel
        ecosystem to be a breath of fresh air. We hope you love it.
    </div>
</div-->
<div class="bg-gray-200 bg-opacity-25 grid grid-cols-1 md:grid-cols-1">
<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="/dashboard">dashboard</a></li>
  <li class="breadcrumb-item active">Create Data</li>
</ol>
    <div class="p-6 border-t border-gray-200 md:border-t-0 md:border-l">
        <div class="flex items-center">
            <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="w-8 h-8 text-gray-400"><path d="M3 9a2 2 0 012-2h.93a2 2 0 001.664-.89l.812-1.22A2 2 0 0110.07 4h3.86a2 2 0 011.664.89l.812 1.22A2 2 0 0018.07 7H19a2 2 0 012 2v9a2 2 0 01-2 2H5a2 2 0 01-2-2V9z"></path><path d="M15 13a3 3 0 11-6 0 3 3 0 016 0z"></path></svg>
            <div class="ml-4 text-lg text-gray-600 leading-7 font-semibold">Create Data</div>
        </div>

        <div class="ml-12">
            <!--form-->
                                    
                                    <form method="post" action="/simpan">
                                    <fieldset>
                                    @csrf
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Email address</label>
                                        <input type="text" value="{{$users->name}}" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="name" placeholder="Nama">
                                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Email address</label>
                                        <input type="text" value="{{$users->id}}" readonly class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="id" placeholder="Nama">
                                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Email address</label>
                                        <input type="text" value="{{$users->suplayer}}" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="suplayer" placeholder="Suplayer">
                                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Email address</label>
                                        <input type="text" value="{{$users->pemesan}}" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="pemesan" placeholder="Pemesan">
                                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                                    </div>
                                        <button type="submit" class="btn btn-primary" name='submit' value='submit'>Submit</button>
                                    </fieldset>
                                    <!--input type="text" name="name" placeholder="nama"></input><br>
                                    <input type="text" name="suplayer" placeholder="Suplayer"></input><br>
                                    <input type="text" name="pemesan" placeholder="Pemesan"></input><br>
                                    <button type="submit" value="submit" name="submit">Simpan</button-->
                                    </form>
                                    
            <!--endform-->


        </div>
    </div>
</div>
<img src="{{asset('storage/app/public/profile-photos/1sRSD6HcAImsfDgvtAHMuLbz5BFE6QnwfZsP1sl0.jpeg')}}"/>

@endsection