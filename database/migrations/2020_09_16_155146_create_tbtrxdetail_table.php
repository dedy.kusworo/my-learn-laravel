<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbtrxdetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbtrxdetail', function (Blueprint $table) {
            $table->id();
            $table->integer('idtrx');
            $table->integer('idproduk');
            $table->string('kodeproduk',16);
            $table->string('namaproduk',32);
            $table->integer('harga');
            $table->string('satuan',16);
            $table->integer('kuantitas');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbtrxdetail');
    }
}
