<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//Route::middleware(['auth:sanctum', 'verified'])->get('/show','App\Http\Controllers\CrudController@index');->> ini digunakan kalau pakai session/
Route::middleware(['auth:sanctum', 'verified'])->get('/show','App\Http\Controllers\CrudController@index')->name('show');
Route::middleware(['auth:sanctum', 'verified'])->get('/create','App\Http\Controllers\CrudController@create')->name('create');
Route::middleware(['auth:sanctum', 'verified'])->post('/create','App\Http\Controllers\CrudController@store');
Route::middleware(['auth:sanctum', 'verified'])->get('/show/{id}','App\Http\Controllers\CrudController@edit');
Route::middleware(['auth:sanctum', 'verified'])->get('/hapus/{id}','App\Http\Controllers\CrudController@destroy');
Route::middleware(['auth:sanctum', 'verified'])->post('/simpan','App\Http\Controllers\CrudController@update');
//produk
Route::middleware(['auth:sanctum', 'verified'])->get('/produk','App\Http\Controllers\ProdukController@index');
Route::middleware(['auth:sanctum', 'verified'])->get('/produk/create','App\Http\Controllers\ProdukController@create');
Route::middleware(['auth:sanctum', 'verified'])->post('/produk/store','App\Http\Controllers\ProdukController@store');
Route::middleware(['auth:sanctum', 'verified'])->get('/produk/edit/{id}','App\Http\Controllers\ProdukController@edit');
Route::middleware(['auth:sanctum', 'verified'])->get('/produk/detail/{id}','App\Http\Controllers\ProdukController@show');
Route::middleware(['auth:sanctum', 'verified'])->post('/produk/simpan','App\Http\Controllers\ProdukController@update');
Route::middleware(['auth:sanctum', 'verified'])->get('/produk/hapus/{id}','App\Http\Controllers\ProdukController@destroy');
//Route::get('/show/{id}','App\Http\Controllers\CrudController@edit');
//transakasi
Route::middleware(['auth:sanctum', 'verified'])->get('/transaksi','App\Http\Controllers\TransaksiController@index');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('screener');
})->name('dashboard');

